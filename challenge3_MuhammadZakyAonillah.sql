-- CHALLANGE 3
-- MUHAMMAD ZAKY AONILLAH
-- BEJS-4

-- CREATE DATABASE

-- CREATE DATABASE challenge3;

-- CREATE TABLE

-- CREATE TABLE user_game(
-- 	id_user INT PRIMARY KEY NOT NULL,
-- 	username VARCHAR(32) NOT NULL,
-- 	password VARCHAR(32) NOT NULL
-- );

-- CREATE TABLE user_game_biodata(
--  	id_biodata INT PRIMARY KEY NOT NULL,
--  	nama_user VARCHAR(100),
--  	jenis_kelamin VARCHAR(50),
--  	umur INT,
--  	email VARCHAR(100),
-- 	id_user INT,
--  	CHECK(jenis_kelamin in ('laki-laki', 'perempuan')),
-- 	FOREIGN KEY(id_user) REFERENCES user_game(id_user)
-- );

-- CREATE TABLE user_game_history(
-- 	id_history INT PRIMARY KEY NOT NULL,
-- 	nama_game VARCHAR(100),
-- 	waktu_bermain TIME,
-- 	skor_game INT,
-- 	id_user INT,
-- 	FOREIGN KEY(id_user) REFERENCES user_game(id_user)
-- );

-- INSERT TABLE

-- INSERT INTO user_game(id_user, username, password)
-- VALUES (1, 'user1', 'user1'), 
-- 	   (2, 'user2', 'user2'),
-- 	   (3, 'user3', 'user3'),
-- 	   (4, 'user4', 'user4'),
-- 	   (5, 'user5', 'user5'),
-- 	   (6, 'user6', 'user6');

-- INSERT INTO user_game_biodata(id_biodata, nama_user, jenis_kelamin, umur, email, id_user)
-- VALUES (1, 'Zaky', 'laki-laki', 22, 'Zaky@gmail.com', 1), 
--  	   (2, 'Rahma', 'perempuan', 21, 'Rahma@gmail.com', 2), 
--  	   (3, 'Dimas', 'laki-laki', 17, 'Dimas@gmail.com', 3), 
--  	   (4, 'Rizqi', 'laki-laki', 16, 'Rizqi@gmail.com', 4), 
--  	   (5, 'Rendi', 'laki-laki', 19, 'Rendi@gmail.com', 5), 
--  	   (6, 'Sukma', 'perempuan', 23, 'Sukma@gmail.com', 6);

-- INSERT INTO user_game_history(id_history, nama_game, waktu_bermain, skor_game, id_user)
-- VALUES (1, 'Valorant', '01:30:00', 10, 1), 
--   	   (2, 'Candy Crush','02:15:30', 8, 1), 
--   	   (3, 'Valorant', '01:30:10', 7,  3), 
--   	   (4, 'PUBG', '02:30:00', 5,  4), 
--   	   (5, 'Mobile Legends', '03:30:15', 9,  5), 
--   	   (6, 'Candy Crush', '01:30:40', 10,  2);
	   

-- SELECT

-- SELECT * FROM user_game;
-- SELECT * FROM user_game_biodata;
-- SELECT * FROM user_game_history;

-- SELECT WITH JOIN

-- Mengambil data user dan biodatanya
-- SELECT * FROM user_game 
-- JOIN user_game_biodata ON user_game.id_user=user_game_biodata.id_user;

-- Mengambil data user dan history game
-- SELECT *
-- FROM user_game
-- JOIN user_game_history ON user_game.id_user=user_game_history.id_user;

-- Mengambil data user, biodata user, dan history game
-- SELECT user_game_biodata.id_user, user_game_biodata.nama_user, user_game_history.nama_game, user_game_history.waktu_bermain
-- FROM user_game_biodata
-- JOIN user_game ON user_game_biodata.id_user=user_game.id_user 
-- JOIN user_game_history ON user_game_biodata.id_user=user_game_history.id_user;


-- UPDATE

-- UPDATE user_game 
-- SET username = 'userVIP1'
-- WHERE username = 'user1';

-- UPDATE user_game_biodata 
-- SET nama_user = 'Muhammad Zaky'
-- WHERE nama_user = 'Zaky';

-- UPDATE user_game_history
-- SET nama_game = 'Candy Crush'
-- WHERE id_user = 4;


-- DELETE USER

-- INSERT INTO user_game(id_user, username, password)
-- VALUES (7, 'user7', 'user7');
-- DELETE FROM user_game
-- WHERE username = 'user7';

--DELETE USER BIODATA

-- INSERT INTO user_game_biodata(id_biodata, nama_user, jenis_kelamin, umur, email, id_user)
-- VALUES (7, 'Zaky', 'laki-laki', 22, 'Zaky@gmail.com', 1); 
-- DELETE FROM user_game_biodata
-- WHERE id_biodata = 7;

-- DELETE USER GAME HISTORY

-- INSERT INTO user_game_history(id_history, nama_game, waktu_bermain, skor_game, id_user)
-- VALUES (7, 'Valorant', '01:30:00', 10, 1);
-- DELETE FROM user_game_history
-- WHERE id_history = 7;
